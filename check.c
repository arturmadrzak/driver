#include <stdlib.h>
#include <check.h>

START_TEST(test_driver_send)
{
}
END_TEST


Suite *driver_suite(void)
{
    Suite *s;
    TCase *tc_core;

    s = suite_create("driver");

    tc_core = tcase_create("core");
    tcase_add_test(tc_core, test_driver_send);

    suite_add_tcase(s, tc_core);

    return s;
}

int main(void)
{
    int number_failed;
    Suite *s;
    SRunner *sr;

    s = driver_suite();
    sr = srunner_create(s);

    srunner_run_all(sr, CK_VERBOSE);
    number_failed = srunner_ntests_failed(sr);
    srunner_free(sr);

    return (number_failed == 0) ? EXIT_SUCCESS : EXIT_FAILURE;
}
