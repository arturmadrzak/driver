#ifndef _DRIVER_H
#define _DRIVER_H

#include "io.h"

/**
 * @brief Incoming message dispatching function
 * @param arg pointer passed in callback configuration
 * @param message buffer containing incoming message
 * @param len message length in bytes
 * */
typedef void (*dispatch_f)(void *arg, void* message, size_t len);

/** @brief driver structure is private. */
typedef void driver_t;

/** @brief Allocacte memory for driver runtime structure, create and start driver's thread. 
 *  @param transport interface for underlaying I/O operations 
 *  @return On success, pointer to allocated driver structure. On error, NULL.*/
driver_t* driver_alloc(io_t *transport);

/** @brief Provide buffer for incoming messages. Must be allocated externaly 
 *  @param r driver structure 
 *  @param buffer memory location for incoming frames 
 *  @param len memory size of input buffer */
void driver_cfg_buffer(driver_t *r, void* buffer, size_t len);

/** @brief Set dispatching function
 *  @param r driver structure
 *  @param callback dispatching function pointer
 *  @param arg pointer to argument passed to dispatching function */
void driver_cfg_dispatcher(driver_t *r, dispatch_f callback, void *arg);

/** @brief Enable driver. Start receive messages, dispatch them, allow command execution 
 *  @param r driver structure 
 *  @returns On success 0, On error -1*/
int driver_open(driver_t *r);

/** @brief Disable driver. Interrupt command execution, put driver in idle mode */
void driver_close(driver_t *r);

/** @brief Send command message and wait for response.
 *
 * Response is interpreted in external dispatch function. This function has to return non 0 value 
 * if expected response is received. */
int driver_write(driver_t *r, void* payload, size_t size);


#endif

