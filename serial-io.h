#ifndef _SERIAL_IO_H
#define _SERIAL_IO_H

#include <stdint.h>
#include <termios.h>
#include "io.h"

io_t* serial_alloc(void);
void serial_config(io_t* io, const char* filename, speed_t speed);


#endif /* _SERIAL_IO_H */
