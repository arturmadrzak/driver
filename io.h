#ifndef _IO_H
#define _IO_H

struct io;
typedef struct io io_t;

struct io {
    int (*open)(io_t *io);
    int (*write)(io_t *io, void *buffer, size_t size);
    int (*read)(io_t *io, void *buffer, size_t size);
    int (*close)(io_t *io);
};

#endif /* _IO_H */

