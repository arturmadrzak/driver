#include <stddef.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>
#include <termios.h>

#include "serial-io.h"

/* for Nuttx compatibility. There is no such function. */
int isatty(int fd) __attribute__((weak));

typedef struct {
    io_t pub;
    int fd;
    const char* filename;
    speed_t speed;
} serial_io_t;

static int m_open(io_t *io)
{
    serial_io_t *sio = (serial_io_t*)io;
    int fd = open(sio->filename, O_RDWR);
	if (fd < 0) {
		perror(sio->filename);
		return fd;
	}
	if(isatty && !isatty(fd)) {
		fprintf(stderr,"File %s is not tty\n", sio->filename);
		return -1;
	}

	struct termios port_settings;
	if (tcgetattr(fd, &port_settings) < 0) {
		fprintf(stderr,"Can't get port settings");
		close(fd);
		return -1;
	}
	port_settings.c_lflag &= ~(ECHO | ECHONL | ICANON | IEXTEN | ISIG);
	port_settings.c_cflag &= ~(CSIZE | PARENB);
	port_settings.c_cflag |= CS8;
	port_settings.c_oflag = 0;
	port_settings.c_iflag &= ~(IGNBRK | BRKINT | ICRNL | INLCR | PARMRK
			| INPCK | ISTRIP | IXON);
	port_settings.c_cc[VMIN] = 1;
	port_settings.c_cc[VTIME] = 0;
	if (cfsetispeed(&port_settings, sio->speed) < 0
		|| cfsetospeed(&port_settings, sio->speed) < 0) {
		fprintf(stderr, "Can't set port speed\n");
		close(fd);
		return -1;
	}
	tcflush(fd, TCIFLUSH);
	if (tcsetattr(fd, TCSANOW, &port_settings) < 0) {
		fprintf(stderr, "Can't apply port settings\n");
		return -1;
	}
	sio->fd =fd;
    return 0;
}

static int m_write(io_t *io, void *buffer, size_t size)
{
    serial_io_t *sio = (serial_io_t*)io;
    return write(sio->fd, buffer, size);
}

static int m_read(io_t *io, void *buffer, size_t size)
{
    serial_io_t *sio = (serial_io_t*)io;
    return read(sio->fd, buffer, size);
}

static int m_close(io_t *io)
{
    serial_io_t *sio = (serial_io_t*)io;
    return close(sio->fd);
}

io_t* serial_alloc() 
{
    serial_io_t* sio = malloc(sizeof(serial_io_t));
    sio->pub.open = m_open;
    sio->pub.write = m_write;
    sio->pub.read = m_read;
    sio->pub.close = m_close;
    sio->fd = -1;
    sio->filename = NULL;
    return (io_t*)sio;
}

void serial_config(io_t* io, const char* filename, speed_t speed)
{
    serial_io_t *sio = (serial_io_t*)io;
    sio->filename = filename;
    sio->speed = speed;
}

