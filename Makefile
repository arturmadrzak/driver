SRCS = driver.c tcp-io.c serial-io.c
OBJS = $(SRCS:.c=.o)
DEPS = $(OBJS:.o=.d)
CFLAGS = -O0 -ggdb3
LDFLAGS = -lpthread 
NAME=driver

.PHONY: all lib clean deps

all: deps lib example 

lib: lib$(NAME).a

example: example.o $(OBJS)

lib$(NAME).a: $(OBJS)
	$(AR) rcs $@ $^

deps: $(OBJS:.o=.d)

check: check.o $(OBJS)
		$(CC) $^ -o $@ $(LDFLAGS) -lcheck -lrt -lm

%.d: %.c
		$(CC) -c $(CFLAGS) -MM -MF $@ $<

clean:
		@rm -rf example
		@rm -rf check
		@rm -rf *.o
		@rm -rf *.d
		@rm -rf *.a

ifneq ($(MAKECMDGOALS), clean)
-include $(OBJS:.o=.d)
endif

