# driver
![build status](https://gitlab.com/madrypl/driver/badges/master/build.svg)  
This module is intended to use in Nuttx RTOS environment, but is compilant with Linux. I needed a universal part of drivers handling different protocols on USART (not only). Classic scanario is to send frame and wait for response, but in some devices unsolicited message can appear on the bus. I decided to use another thread to receive and dispatch messages. Beside this, there are two IO modules. One for USART/serial port another one is for TCP/IP.

## usage
The basic concept and flow is shown bellow snippet.
```c
    handle_incoming_frame(frame) {
    //do stuff with income frame
    }
    ...
    driver_alloc()           // allocate memory for internal state
    driver_cfg_buffer();     // buffer memory is provided externally
    driver_cfg_dispatcher(handle_incoming_frame); // set callback for incoming frames
    driver_open();           // start thread and wait for incoming frames
    while(1);                // do whatever needed in main thread
    driver_close();          // stop reading incoming messages and kill reader thread
    free(d);                 // free allocated memory
```
For more detailed usage information, refer please documentation in source code.

## _io_t_ interface
Assumption with this interface is that, ```read```/```write``` methods operates on whole frames. In very basic example the frame is considered just as single byte. This behaviour is implemented in ```serial-io``` and ```tcp-io```. But, in most, if not all, cases frames are more complicated. For example, AT protocol frame has a form:
```
AT...\r\n
```
so there is a need to implement ```io_t``` encoder, which returns from read metod only when receive whole sentence.

## build
By default make produces static library(```libdriver.a```) and ```example```. I built for x86_64 and arm-none-eabi targets using clang and gcc. To build all boils down to:
```
git clone https://gitlab.com/madrypl/driver.git 
cd driver
make
```

## example
For now the best example is another repostiory: https://gitlab.com/madrypl/gps-osp


## license
See  LICENSE file
