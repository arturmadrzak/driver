#include <stddef.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>

#include "tcp-io.h"

typedef struct {
    io_t pub;
    int fd;
    const char* host;
    uint16_t port;
} tcp_io_t;

static int m_open(io_t *io)
{
    tcp_io_t *tio = (tcp_io_t*)io;
    struct sockaddr_in addr;
    struct hostent *server;
    int fd;
    printf("tcp-io: opening: %s:%d\n", tio->host, tio->port);
    if ((fd = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
        return -1;
    }
    server = gethostbyname(tio->host);
    bzero((char*)&addr, sizeof(addr));
    addr.sin_family = AF_INET;
    bcopy((char*) server->h_addr, (char*)&addr.sin_addr.s_addr, server->h_length);
    addr.sin_port = htons(tio->port);
    if (connect(fd, (struct sockaddr*)&addr, sizeof(addr)) < 0) {
        return -1;
    }
    tio->fd = fd;
    return 0;
}

static int m_write(io_t *io, void *buffer, size_t size)
{
    tcp_io_t *tio = (tcp_io_t*)io;
    return write(tio->fd, buffer, size);
}

static int m_read(io_t *io, void *buffer, size_t size)
{
    tcp_io_t *tio = (tcp_io_t*)io;
    return read(tio->fd, buffer, size);
}

static int m_close(io_t *io)
{
    tcp_io_t *tio = (tcp_io_t*)io;
    return close(tio->fd);
}

io_t* tcp_alloc() 
{
    tcp_io_t* tio = malloc(sizeof(tcp_io_t));
    tio->pub.open = m_open;
    tio->pub.write = m_write;
    tio->pub.read = m_read;
    tio->pub.close = m_close;
    tio->fd = -1;
    tio->host = NULL;
    tio->port = 0;
    return (io_t*)tio;
}

void tcp_destroy(io_t* io)
{
    free(io);
}

void tcp_config(io_t* io, char* host, uint16_t port)
{
    tcp_io_t *tio = (tcp_io_t*)io;
    tio->host = host;
    tio->port = port;
}

