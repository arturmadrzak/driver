#include <stdio.h>
#include <stddef.h>
#include <stdbool.h>
#include <stdlib.h>
#include <unistd.h>
#include <pthread.h>
#include <time.h>
#include <signal.h>

#include "driver.h"

typedef struct {
    pthread_t handle;
    pthread_mutex_t lock;
    pthread_cond_t signal;
    void *buffer;
    size_t size;
    io_t *io;
    dispatch_f dispatch;
    void *dispatch_arg;
    bool running;
} module_t;

static void* runtime(void *arg)
{
    module_t *m = (module_t*)arg;
    pthread_mutex_lock(&m->lock);
    do {
        if (!m->io) {
            fprintf(stderr, "driver: IO is not configured\n");
            m->running = false;
            pthread_cond_signal(&m->signal);
            pthread_mutex_unlock(&m->lock);
            return NULL;
        }
        if (m->io->open(m->io)) {
            m->running = false;
            pthread_cond_signal(&m->signal);
            pthread_mutex_unlock(&m->lock);
            return NULL;
        }

        m->running =  true;
        pthread_cond_signal(&m->signal);
        pthread_mutex_unlock(&m->lock);
        while(m->running) {
            ssize_t r;
            if ((r = m->io->read(m->io, m->buffer, m->size)) < 0) {
                fprintf(stderr, "driver: read failed\n");
                break;
            }
            if (m->dispatch) m->dispatch(m->dispatch_arg, m->buffer, r);
        }
        m->io->close(m->io);
    } while (m->running);
    return NULL;
}

driver_t* driver_alloc(io_t *transport)
{
    module_t *m;
    m = calloc(1, sizeof(module_t));
    m->io = transport;
    return (driver_t*) m;
}

void driver_cfg_buffer(driver_t *r, void* buffer, size_t len)
{
    module_t *m = (module_t*)r;
    m->buffer = buffer;
    m->size = len;
}

void driver_cfg_dispatcher(driver_t *r, dispatch_f d, void *arg)
{
    module_t *m = (module_t*)r;
    m->dispatch = d;
    m->dispatch_arg = arg;
}

int driver_open(driver_t *r)
{
    module_t *m = (module_t*)r;
    bool started = false;
    pthread_mutex_init(&m->lock, NULL);
    pthread_cond_init(&m->signal, NULL);
    
    pthread_attr_t attr;
    pthread_attr_init(&attr);
    pthread_create(&m->handle, &attr, runtime, m);
    pthread_attr_destroy(&attr);
 
    /* ensure worker thread is running */
    pthread_mutex_lock(&m->lock);
    pthread_cond_wait(&m->signal, &m->lock);
    started = m->running;
    pthread_mutex_unlock(&m->lock);
    return started ? 0 : -1;
}

void driver_close(driver_t *r)
{
    module_t *m = (module_t*)r;
    pthread_mutex_lock(&m->lock);
    m->running = false;
    pthread_mutex_unlock(&m->lock);

    pthread_kill(m->handle, SIGUSR1);
    pthread_join(m->handle, NULL);
    pthread_mutex_destroy(&m->lock);
    pthread_cond_destroy(&m->signal);
}

int driver_write(driver_t *r, void* payload, size_t size)
{
    int err;
    module_t *m = (module_t*)r;
    pthread_mutex_lock(&m->lock);
    err = m->io->write(m->io, payload, size);
    pthread_mutex_unlock(&m->lock);
    return err;
}

