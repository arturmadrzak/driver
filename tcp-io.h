#ifndef _TCP_IO_H
#define _TCP_IO_H

#include <stdint.h>
#include "io.h"

io_t* tcp_alloc(void);
void tcp_destroy(io_t* io);
void tcp_config(io_t* io, char* host, uint16_t port);


#endif /* _TCP_IO_H */
